import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { AuthService } from '../services/auth/auth.service';
import { environment } from '../../environments/environment';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.sass']
})
export class LoginComponent implements OnInit {
  lForm: FormGroup;
  errorMsg = null;

  constructor(private fb: FormBuilder,
              private authService: AuthService,
              private router: Router) {
    authService.logout();
    this.lForm = new FormGroup({
      username: new FormControl('', Validators.required),
      password: new FormControl('', Validators.required)
    });
  }

  login() {
    this.authService.login(this.lForm.value).subscribe(
      result => {
        // Получаем информацию о пользователе
        this.authService.getUserInfo().subscribe(
          userData => {
            this.authService.userInfo = userData;
            localStorage.setItem('userInfo', JSON.stringify(userData));
            this.router.navigate(['/index']);
          }
        );
      },
      error => {
        if (error.status === 401) {
          this.errorMsg = 'Вы ввели неверное имя пользователя или пароль';
        }else if (error.status === 0) {
          this.errorMsg = 'Ошибка соединения с сервером';
        }else {
          this.errorMsg = 'Ошибка';
        }
      }
    );
  }

  ngOnInit() {
  }

}
