import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewFsspFileComponent } from './view-fssp-file.component';

describe('ViewFsspFileComponent', () => {
  let component: ViewFsspFileComponent;
  let fixture: ComponentFixture<ViewFsspFileComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewFsspFileComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewFsspFileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
