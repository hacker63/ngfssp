import { Component, OnInit } from '@angular/core';
import { FsspFileService } from '../services/fssp-file/fssp-file.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-view-fssp-file',
  templateUrl: './view-fssp-file.component.html',
  styleUrls: ['./view-fssp-file.component.sass']
})
export class ViewFsspFileComponent implements OnInit {
  package_id = null;
  constructor(
    private router: Router,
    private route: ActivatedRoute
  ) {
    this.package_id = route.snapshot.params.id;
  }

  ngOnInit() {
  }

}
