import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpModule, Http } from '@angular/http';
import { ReactiveFormsModule } from '@angular/forms';
import { RouterModule, Router, Routes } from '@angular/router';

// Сервисы
import { AuthService } from './services/auth/auth.service';
import { DebtorService } from './services/debtor/debtor.service';
import { FsspFileService } from './services/fssp-file/fssp-file.service';

// Guards
import { AuthGuard } from './guards/auth/auth.guard';

import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { HomeComponent } from './home/home.component';
import { NavbarComponent } from './navbar/navbar.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { ProfileComponent } from './profile/profile.component';
import { ListDebtorsComponent } from './list-debtors/list-debtors.component';
import { ListPackagesComponent } from './list-packages/list-packages.component';
import { StatsComponent } from './stats/stats.component';
import { StatInComponent } from './stats/stat-in/stat-in.component';
import { StatOutComponent } from './stats/stat-out/stat-out.component';
import { DebtorViewComponent } from './debtor-view/debtor-view.component';
import { SearchDebtorsComponent } from './search-debtors/search-debtors.component';
import { FsspFilesComponent } from './fssp-files/fssp-files.component';
import { ViewFsspFileComponent } from './view-fssp-file/view-fssp-file.component';

const routes: Routes = [
  {
    path: 'login',
    component: LoginComponent
  },
  {
    path: '',
    children: [
      {
        path: '',
        redirectTo: '/index',
        pathMatch: 'full'
      },
      {
        path: 'index',
        component: HomeComponent,
      },
      {
        path: 'profile',
        component: ProfileComponent,
      },
      {
        path: 'packages',
        children: [
          {
            path: '',
            component: ListPackagesComponent
          },
          {
            path: ':id',
            children: [
              {
                path: 'debtors',
                children: [
                  {
                    path: '',
                    component: ListDebtorsComponent
                  },
                  {
                    path: ':id',
                    component: DebtorViewComponent
                  }
                ]
              },
              {
                path: 'search',
                component: SearchDebtorsComponent,
              },
              {
                path: 'files',
                children: [
                  {
                    path: '',
                    component: FsspFilesComponent
                  },
                  {
                    path: ':id',
                    component: ViewFsspFileComponent
                  }
                ]
              },
              {
                path: ':id',
                component: DebtorViewComponent
              },
            ]
          },
        ]
      },
      {
        path: 'stats',
        component: StatsComponent,
        children: [
          { path: '', redirectTo: 'in', pathMatch: 'full'},
          { path: 'in', component: StatInComponent},
          { path: 'out', component: StatOutComponent}
        ]
      },
      {
        path: '**',
        component: PageNotFoundComponent
      }
    ],
    canActivate: [AuthGuard]
  },
];

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    HomeComponent,
    NavbarComponent,
    PageNotFoundComponent,
    ProfileComponent,
    ListDebtorsComponent,
    ListPackagesComponent,
    StatsComponent,
    StatInComponent,
    StatOutComponent,
    DebtorViewComponent,
    SearchDebtorsComponent,
    FsspFilesComponent,
    ViewFsspFileComponent,
  ],
  imports: [
    BrowserModule,
    HttpModule,
    RouterModule.forRoot(routes),
    ReactiveFormsModule
  ],
  providers: [AuthService, DebtorService, FsspFileService, AuthGuard],
  bootstrap: [AppComponent]
})
export class AppModule { }
