import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { DebtorService } from '../services/debtor/debtor.service';
import { FormControl, FormGroup, FormBuilder } from '@angular/forms';
import { AuthService } from '../services/auth/auth.service';
import { environment } from '../../environments/environment';

@Component({
  selector: 'app-debtor-view',
  templateUrl: './debtor-view.component.html',
  styleUrls: ['./debtor-view.component.sass']
})
export class DebtorViewComponent implements OnInit {
  URLServer = environment.urlApi;
  identForm: FormGroup;
  identStatusList = [];
  debtor: any = null;
  loading = true;
  errorMsg = null;
  eventEditIdentStatus = false;
  loadingIdentStatus = false;
  myUser = {
    id: null,
    username: null,
    email: null,
    first_name: null,
    last_name: null
  }
  constructor(
    private route: ActivatedRoute,
    private ds: DebtorService,
    private fb: FormBuilder,
    private authService: AuthService
  ) {
    this.myUser = this.authService.userInfo;
    ds.getIdentStatusList().subscribe(
      statusList => {
        this.identStatusList = statusList;
      }
    );
    route.params.subscribe(
      params => {
        ds.getDebtor(params['id']).subscribe(
          result => {
            this.debtor = result;
            this.identForm = this.fb.group({
              status: this.fb.control({value: this.debtor.IDENT_STATUS.id, disabled: true}),
              comment: this.fb.control({value: this.debtor.IDENT_STATUS_COMMENT, disabled: true})
            });
          },
          error => {
            if(error.status == 0){
              this.errorMsg = 'Ошибка соединения с сервером';
              this.loading = false;
            }
          },
          () => {
            this.loading = false;
          }
        );
      }
    );
  }

  toComplete() {
    this.ds.toComplete(this.debtor.ID).subscribe(
      result => {
        this.debtor = result;
      },
      error => {
        if( error.status == 400 ) {
          this.ds.getDebtor(this.debtor.ID).subscribe(
            result => {
              this.debtor = result;
              this.loading = false;
            }
          );
        }
        this.errorMsg = error.json().error;
      }
    );
  }

  toProcess() {
    this.ds.toProcess(this.debtor.ID).subscribe(
      result => {
        this.debtor = result;
      },
      error => {
        if( error.status == 400 ) {
          this.ds.getDebtor(this.debtor.ID).subscribe(
            result => {
              this.debtor = result;
              this.loading = false;
            }
          );
        }
        this.errorMsg = error.json().error;
      }
    );
  }

  toCancel() {
    if(confirm('Вы хотите отказаться от обработки?')){
      this.ds.toCancel(this.debtor.ID).subscribe(
        result => {
          this.debtor = result;
        },
        error => {
          if( error.status == 400 ) {
            this.ds.getDebtor(this.debtor.ID).subscribe(
              result => {
                this.debtor = result;
                this.loading = false;
              }
            );
          }
          this.errorMsg = error.json().error;
        }
      );
    }
  }

  sendIdentStatus() {
    this.loadingIdentStatus = true;
    let postData = {}
    postData['IDENT_STATUS'] = this.identForm.value.status,
    postData['IDENT_STATUS_COMMENT'] = this.identForm.value.comment;
    if(postData['IDENT_STATUS_COMMENT']) {
      postData['IDENT_STATUS_COMMENT'] = postData['IDENT_STATUS_COMMENT'].trim();
    }
    this.ds.setIdentStatus(this.debtor.ID, postData).subscribe(
      debtor => {
        this.debtor = debtor;
      },
      error => {
        this.identForm.reset(
          {
            status: this.debtor.IDENT_STATUS.id,
            comment: this.debtor.IDENT_STATUS_COMMENT
          });
      },
      () => {
        this.loadingIdentStatus = false;
      }
    );
    this.identForm.disable();
    this.eventEditIdentStatus = false;
  }

  resetFormComment() {
    this.eventEditIdentStatus = false;
    this.identForm.reset(
      {
        status: this.debtor.IDENT_STATUS.id,
        comment: this.debtor.IDENT_STATUS_COMMENT
      });
    this.identForm.disable();
  }

  editIdentStatus() {
    this.eventEditIdentStatus = true;
    this.identForm.enable();
  }

  ngOnInit() {
  }

}
