import { Injectable } from '@angular/core';
import { AuthService } from '../auth/auth.service';
import { Http, RequestOptions, Headers, ResponseContentType } from '@angular/http';
import { Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class FsspFileService {

  constructor(
    private authService: AuthService,
    private http: Http,
    private router: Router
  ) { }

  getFileList(package_id, page=null) {
    const headers = new Headers;
    headers.append('Content-Type', 'application/json');
    headers.append('Authorization', this.authService.authInfo.token_type + ' ' + this.authService.authInfo.access_token);
    const options = new RequestOptions({
      headers: headers
    });
    let resultUrl = this.authService.urlApi + 'api/v1/packages/' + package_id + '/files';
    if(page){
      resultUrl += '?page=' + page;
    }
    return this.http.get(resultUrl, options)
                    .map(response => response.json())
                    .catch(error => this.handleError(error));
  }

  downloadFile(file_id) {
    let options = {responseType: ResponseContentType.ArrayBuffer };
    let resultUrl = this.authService.urlApi + 'files/' + file_id;
    return this.http.get(resultUrl, options)
    .catch(error => this.handleError(error));
  }

  handleError(error) {
    if(error.status == 0 || error.status == 401){
      return this.router.navigate(['/login']);
    }
    return Observable.throw(error);
  }
}
