import { TestBed, inject } from '@angular/core/testing';

import { FsspFileService } from './fssp-file.service';

describe('FsspFileService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [FsspFileService]
    });
  });

  it('should be created', inject([FsspFileService], (service: FsspFileService) => {
    expect(service).toBeTruthy();
  }));
});
