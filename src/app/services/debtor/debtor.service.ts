import { Injectable } from '@angular/core';
import { AuthService } from '../auth/auth.service';
import { Http, Headers, RequestOptions, ResponseContentType } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { Router } from '@angular/router';

@Injectable()
export class DebtorService {

  constructor(private authService: AuthService,
              private http: Http,
              private router: Router
            ) {}

  getDebtorsList(id_package, page=null, params= null) {
    const headers = new Headers;
    headers.append('Content-Type', 'application/json');
    headers.append('Authorization', this.authService.authInfo.token_type + ' ' + this.authService.authInfo.access_token);
    const options = new RequestOptions({
      headers: headers
    });
    let resultURL = this.authService.urlApi + 'api/v1/packages/' + id_package;
    if(page) {
      resultURL += '?page=' + page;
    }
    return this.http.get(resultURL, options)
      .map(response => response.json())
      .catch(error => this.handleError(error));
  }

  getPackages(getURL=null) {
    const headers = new Headers;
    headers.append('Content-Type', 'application/json');
    headers.append('Authorization', this.authService.authInfo.token_type + ' ' + this.authService.authInfo.access_token);
    const options = new RequestOptions({
      headers: headers
    });
    let resultURL;
    if(getURL) {
      resultURL = getURL;
    }else{
      resultURL = this.authService.urlApi + 'api/v1/packages/';
    }
    return this.http.get(resultURL, options)
                    .map(response => response.json())
                    .catch(error => this.handleError(error));
  }

  getPacket(id=null) {
    const headers = new Headers;
    headers.append('Content-Type', 'application/json');
    headers.append('Authorization', this.authService.authInfo.token_type + ' ' + this.authService.authInfo.access_token);
    const options = new RequestOptions({
      headers: headers
    });
    return this.http.get(this.authService.urlApi + 'api/v1/package/' + id, options)
                    .map(response => response.json())
                    .catch(error => this.handleError(error));
  }

  getDebtor(id=null) {
    const headers = new Headers;
    headers.append('Content-Type', 'application/json');
    headers.append('Authorization', this.authService.authInfo.token_type + ' ' + this.authService.authInfo.access_token);
    const options = new RequestOptions({
      headers: headers
    });
    return this.http.get(this.authService.urlApi + 'api/v1/debtors/' + id, options)
                    .map(response => response.json())
                    .catch(error => this.handleError(error));
  }

  toComplete(id= null) {
    const headers = new Headers;
    headers.append('Content-Type', 'application/json');
    headers.append('Authorization', this.authService.authInfo.token_type + ' ' + this.authService.authInfo.access_token);
    const options = new RequestOptions({
      headers: headers
    });
    return this.http.get(this.authService.urlApi + 'api/v1/debtors/' + id + '/toComplete/', options)
                    .map(response => response.json())
                    .catch(error => this.handleError(error));
  }

  toProcess(id= null) {
    const headers = new Headers;
    headers.append('Content-Type', 'application/json');
    headers.append('Authorization', this.authService.authInfo.token_type + ' ' + this.authService.authInfo.access_token);
    const options = new RequestOptions({
      headers: headers
    });
    return this.http.get(this.authService.urlApi + 'api/v1/debtors/' + id + '/toProcess/', options)
                    .map(response => response.json())
                    .catch(error => this.handleError(error));
  }

  toCancel(id=null) {
    const headers = new Headers;
    headers.append('Content-Type', 'application/json');
    headers.append('Authorization', this.authService.authInfo.token_type + ' ' + this.authService.authInfo.access_token);
    const options = new RequestOptions({
      headers: headers
    });
    return this.http.get(this.authService.urlApi + 'api/v1/debtors/' + id + '/toCancel/', options)
                    .map(response => response.json())
                    .catch(error => this.handleError(error));
  }

  getIdentStatusList(id=null) {
    const headers = new Headers;
    headers.append('Content-Type', 'application/json');
    headers.append('Authorization', this.authService.authInfo.token_type + ' ' + this.authService.authInfo.access_token);
    const options = new RequestOptions({
      headers: headers
    });
    return this.http.get(this.authService.urlApi + 'api/v1/ident-status/list/', options)
                    .map(response => response.json())
                    .catch(error => this.handleError(error));
  }

  setIdentStatus(debtor_id,postData) {
    const headers = new Headers;
    headers.append('Content-Type', 'application/json');
    headers.append('Authorization', this.authService.authInfo.token_type + ' ' + this.authService.authInfo.access_token);
    const options = new RequestOptions({
      headers: headers
    });
    return this.http.post(this.authService.urlApi + 'api/v1/debtors/' + debtor_id + '/edit-status-ident/', postData, options)
                    .map(response => response.json())
                    .catch(error => this.handleError(error));
  }

  genPackageReport(id_package=null) {
    const headers = new Headers;
    headers.append('Content-Type', 'application/json');
    headers.append('Authorization', this.authService.authInfo.token_type + ' ' + this.authService.authInfo.access_token);
    const options = new RequestOptions({
      headers: headers
    });
    return this.http.get(this.authService.urlApi + 'api/v1/packages/' + id_package + '/generate-report', options)
                    .map(response => response.json())
                    .catch(error => this.handleError(error));
  }

  getRayons() {
    const headers = new Headers;
    headers.append('Content-Type', 'application/json');
    headers.append('Authorization', this.authService.authInfo.token_type + ' ' + this.authService.authInfo.access_token);
    const options = new RequestOptions({
      headers: headers
    });
    return this.http.get(this.authService.urlApi + 'api/v1/rayons/', options)
                    .map(response => response.json())
                    .catch(error => this.handleError(error));
  }

  search(package_id, form_values, pageNum=null) {
    const headers = new Headers;
    headers.append('Content-Type', 'application/json');
    headers.append('Authorization', this.authService.authInfo.token_type + ' ' + this.authService.authInfo.access_token);
    const options = new RequestOptions({
      headers: headers
    });
    let resultUrl = this.authService.urlApi + 'api/v1/packages/' + package_id;
    if(pageNum){
      resultUrl += '?page=' + pageNum
    }
    return this.http.post(resultUrl, form_values, options)
                    .map(response => response.json())
                    .catch(error => this.handleError(error));
  }

  downloadReport(package_id) {
    let options = {responseType: ResponseContentType.ArrayBuffer };
    let resultUrl = this.authService.urlApi + 'packages/' + package_id + '/generate-report/';
    return this.http.get(resultUrl, options)
    .catch(error => this.handleError(error));
  }

  handleError(error) {
    if(error.status == 0 || error.status == 401){
      return this.router.navigate(['/login']);
    }
    return Observable.throw(error);
  }
}
