import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions, Response } from '@angular/http';
import { environment } from '../../../environments/environment';
import { Observable } from 'rxjs/Observable';
import { Router } from '@angular/router';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/of';
import 'rxjs/add/observable/throw';

@Injectable()
export class AuthService {
  public urlApi = environment.urlApi;
  client_id = environment.client_id;
  client_secret = environment.client_secret;
  public userInfo = {
    id: null,
    username: null,
    last_name: null,
    first_name: null,
    email: null
  };
  public authInfo = {
    access_token: null,
    expires_in: null,
    refresh_token: null,
    scope: null,
    token_type: null,
    date_expired: null
  };

  constructor(private http: Http,
              private router: Router) {
    if (localStorage.getItem('authInfo')) {
      this.authInfo = JSON.parse(localStorage.getItem('authInfo'));
      this.authInfo['date_expired'] = new Date(this.authInfo['date_expired']);
      if (localStorage.getItem('userInfo')) {
        this.userInfo = JSON.parse(localStorage.getItem('userInfo'));
      }else {
        this.getUserInfo().subscribe(
          result => {
            this.userInfo = result;
          },
          error => {
            this.router.navigate(['/login']);
          }
        );
      }
    }
  }

  login(loginData): Observable<any> {
    const headers = new Headers();
    headers.append('Content-type', 'application/x-www-form-urlencoded');
    const options = new RequestOptions({
      headers: headers
    });
    let resultUrl = this.urlApi + 'o/token/';
    resultUrl += '?grant_type=password';
    resultUrl += '&client_id=' + this.client_id;
    resultUrl += '&client_secret=' + this.client_secret;
    resultUrl += '&username=' + loginData.username;
    resultUrl += '&password=' + loginData.password;
    return this.http.post(resultUrl, options)
                    .map(response => response.json())
                    .map(
                      result => {
                        const authInfo = result;
                        authInfo.date_expired = new Date(new Date().getTime() + authInfo.expires_in * 1000);
                        this.authInfo = authInfo;
                        localStorage.setItem('authInfo', JSON.stringify(authInfo));
                      }
                    )
                    .catch(error => this.handleError(error));
  }

  verifyToken(): Observable<boolean> {
    if (this.authInfo.date_expired.getTime() > new Date().getTime()) {
      return Observable.of(true);
    }else {
      return this.refreshToken();
    }
  }

  refreshToken(): Observable<any> {
    const headers = new Headers;
    const options = new RequestOptions({
      headers: headers
    });
    let resultURL = this.urlApi;
    resultURL += 'o/token/';
    resultURL += '?grant_type=refresh_token';
    resultURL += '&client_id=' + this.client_id;
    resultURL += '&client_secret=' + this.client_secret;
    resultURL += '&refresh_token=' + this.authInfo.refresh_token;
    headers.append('Content-type', 'application/json');
    headers.append('Authorization', this.authInfo.token_type + ' ' + this.authInfo.access_token);
    return this.http.post(resultURL, options)
      .map((response: Response) => {
        // Успешный логин
        const authInfo = response.json();
        authInfo.date_expired = new Date(new Date().getTime() + authInfo.expires_in * 1000);
        const token = response.json() && response.json().access_token;
        if (token) {
          this.authInfo = authInfo;
          localStorage.setItem('authInfo', JSON.stringify(authInfo));
          // оповещеие об успешном логине.
          return true;
        }
        return false;
      })
      .catch(function(error: Response | any) {
        return Observable.of(false);
      });
  }

  getUserInfo() {
    const headers = new Headers();
    headers.append('Content-type', 'application/json');
    headers.append('Authorization', this.authInfo.token_type + ' ' + this.authInfo.access_token);
    const options = {
      headers: headers
    };
    return this.http.get(this.urlApi + 'api/v1/my_profile', options)
                    .map(result => result.json())
                    .catch(error => this.handleError(error));
  }

  logout() {
    this.authInfo = null;
    localStorage.removeItem('authInfo');
    this.userInfo = null;
    localStorage.removeItem('userInfo');
  }

  handleError(error) {
    if(error.status == 0 || error.status == 401){
      return this.router.navigate(['/login']);
    }
    return Observable.throw(error);
  }
}
