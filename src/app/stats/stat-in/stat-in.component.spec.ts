import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StatInComponent } from './stat-in.component';

describe('StatInComponent', () => {
  let component: StatInComponent;
  let fixture: ComponentFixture<StatInComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StatInComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StatInComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
