import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StatOutComponent } from './stat-out.component';

describe('StatOutComponent', () => {
  let component: StatOutComponent;
  let fixture: ComponentFixture<StatOutComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StatOutComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StatOutComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
