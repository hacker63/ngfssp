import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { AuthService } from '../services/auth/auth.service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.sass']
})
export class ProfileComponent implements OnInit {
  loading: boolean;
  pForm: FormGroup;
  myUser = {
    id: null,
    username: null,
    last_name: null,
    first_name: null,
    email: null
  };
  constructor(private authService: AuthService) {
    this.loading = true;
    this.myUser = authService.userInfo;
    this.pForm = new FormGroup({
      id: new FormControl({value: this.myUser.id, disabled: true}),
      username: new FormControl({value: this.myUser.username, disabled: true}),
      lastName: new FormControl({value: this.myUser.last_name, disabled: true}),
      firstName: new FormControl({value: this.myUser.first_name, disabled: true}),
      email: new FormControl({value: this.myUser.email, disabled: true})
    });
    this.loading = false;
  }

  ngOnInit() {
  }

}
