import { Component, OnInit } from '@angular/core';
import { DebtorService } from '../services/debtor/debtor.service';
import { ActivatedRoute } from '@angular/router';
import { environment } from '../../environments/environment';
import { FormGroup, FormBuilder, FormArray, Validators } from '@angular/forms';

@Component({
  selector: 'app-list-debtors',
  templateUrl: './list-debtors.component.html',
  styleUrls: ['./list-debtors.component.sass']
})
export class ListDebtorsComponent implements OnInit {
  searchForm: FormGroup;
  packet = {
    ID: null,
    TS: null,
    TOTAL_FILES_COUNT: null
  };
  URLServer = environment.urlApi;
  listDebtors = {
    count: null,
    next: null,
    previous: null,
    current: null,
    of: null,
    results: []
  };
  searchMod = false;
  rayons = [];
  error;
  loading = true;
  private sub: any;
  constructor(private ds: DebtorService,
              private route: ActivatedRoute,
              private fb: FormBuilder
            ) {
    this.searchForm = fb.group({
      RA: fb.control(null)
    });
    ds.getRayons().subscribe(
      result => {
        this.rayons = result;
      }
    );
    this.sub = this.route.params.subscribe(
      params => {
        ds.getPacket(params['id']).subscribe(
          packet => {
            this.packet = packet;
            this.getListDebtors();
          }
        );
      }
    );
  }

  getReset(){
    this.searchMod = false;
    this.loading = true;
    this.searchForm.reset();
    this.getListDebtors();
  }

  search(pageNum=null) {
    this.searchMod = true;
    this.loading = true;
    this.error = null;
    if(pageNum) {
      this.ds.search(this.packet.ID, this.searchForm.value, pageNum).subscribe(
        result => {
          this.listDebtors = result;
          this.loading = false;
        },
        error => {
          console.log(error);
          if(error.status == 0){
            this.error = 'Ошибка соединения с сервером';
          }else {
            this.error = 'Ошибка при получениие списка должников';
          }
          this.loading = false;
        }
      );
    }else {
      this.ds.search(this.packet.ID, this.searchForm.value).subscribe(
        result => {
          this.listDebtors = result;
          this.loading = false;
        },
        error => {
          console.log(error);
          if(error.status == 0){
            this.error = 'Ошибка соединения с сервером';
          }else {
            this.error = 'Ошибка при получениие списка должников';
          }
          this.loading = false;
        }
      );
    }
  }

  hasNextPage(event) {
    event.preventDefault();
    if(this.listDebtors.next){
      if(this.searchMod){
        this.search(this.listDebtors.next)
      }else {
        this.getListDebtors(this.listDebtors.next);
      }
    }
  }

  hasPreviousPage(event) {
    event.preventDefault();
    if(this.listDebtors.previous){
      if(this.searchMod){
        this.search(this.listDebtors.previous);
      }else {
        this.getListDebtors(this.listDebtors.previous);
      }
    }
  }

  getListDebtors(page= null, params= null) {
    this.error = null;
    this.loading = true;
    this.ds.getDebtorsList(this.packet.ID, page, params).subscribe(
      result => {
        this.listDebtors = result;
        this.loading = false;
      },
      error => {
        console.log(error);
        if(error.status == 0){
          this.error = 'Ошибка соединения с сервером';
        }else {
          this.error = 'Ошибка при получениие списка должников';
        }
        this.loading = false;
      }
    );
  }

  ngOnInit() {
    this.sub.unsubscribe();
  }

}
