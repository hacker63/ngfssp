import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, FormArray } from '@angular/forms';
import { DebtorService } from '../services/debtor/debtor.service';
import { Observable } from 'rxjs/Observable';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-search-debtors',
  templateUrl: './search-debtors.component.html',
  styleUrls: ['./search-debtors.component.sass']
})
export class SearchDebtorsComponent implements OnInit {
  private sub: any;
  loading: boolean = false;
  package_id = null;
  searchForm: FormGroup;
  raList = [];
  identStatusList = [];
  listDebtorsLoading: any = null;
  listDebtors = {
    count: null,
    next: null,
    previous: null,
    current: null,
    of: null,
    results: []
  }
  constructor(
    private fb: FormBuilder,
    private ds: DebtorService,
    private route: ActivatedRoute
  ) {
    this.sub = this.route.params.subscribe(
      params => {
        this.package_id = params['id'];
      });
    ds.getRayons().subscribe(
      rayons => {
        this.raList = rayons;
      }
    );
    ds.getIdentStatusList().subscribe(
      statusList => {
        this.identStatusList = statusList;
      }
    );
    this.searchForm = fb.group({
      RA: fb.control(null),
      IDENT_STATUS: fb.control(null),
      IM: fb.control(null),
      FA: fb.control(null),
      OT: fb.control(null),
      RDAT: fb.control(null),
      SEARCH_OUT: fb.control(false)
    });
    this.loading = false;
  }

  search(pageNum= null) {
    this.listDebtorsLoading = true;
    const dataForm = this.searchForm.value;
    console.log(dataForm);
    if(pageNum){
      this.ds.search(this.package_id,dataForm, pageNum).subscribe(
        result => {
          this.listDebtors = result;
        },
        error => {
          this.listDebtorsLoading = false;
        },
        () => {
          this.listDebtorsLoading = false;
        }
      );
    }else {
      this.ds.search(this.package_id,dataForm).subscribe(
        result => {
          this.listDebtors = result;
        },
        error => {
          this.listDebtorsLoading = false;
        },
        () => {
          this.listDebtorsLoading = false;
        }
      );
    }
  }
  
  hasNextPage(event) {
    event.preventDefault();
    if(this.listDebtors.next){
      this.search(this.listDebtors.next)
    }
  }

  hasPreviousPage(event) {
    event.preventDefault();
    if(this.listDebtors.previous){
      this.search(this.listDebtors.next);
    }
  }

  getReset() {
    this.searchForm.reset({SEARCH_OUT: false});
    this.listDebtors = {
      count: null,
      next: null,
      previous: null,
      current: null,
      of: null,
      results: []
    }
    this.listDebtorsLoading = null;
  }

  ngOnInit() {
  }

}
