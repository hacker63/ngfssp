import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SearchDebtorsComponent } from './search-debtors.component';

describe('SearchDebtorsComponent', () => {
  let component: SearchDebtorsComponent;
  let fixture: ComponentFixture<SearchDebtorsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SearchDebtorsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SearchDebtorsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
