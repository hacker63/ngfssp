import { Component, OnInit } from '@angular/core';
import { DebtorService } from '../services/debtor/debtor.service';
import { environment } from '../../environments/environment';
import { saveAs } from 'file-saver';

@Component({
  selector: 'app-list-packages',
  templateUrl: './list-packages.component.html',
  styleUrls: ['./list-packages.component.sass']
})
export class ListPackagesComponent implements OnInit {
  loading = true;
  apiURL = environment.urlApi;
  error: any = false;
  packages = {
    count: null,
    next: null,
    previous: null,
    results: []
  };
  constructor(private ds: DebtorService) {
    ds.getPackages().subscribe(
      result => {
        this.packages = result;
        this.loading = false;
      },
      error => {
        if(error.status == 0){
          this.error = 'Ошибка соединения с сервером';
        }else {
          this.error = 'Пакеты с должниками отсутствуют в базе';
        }
        this.loading = false;
      }
    );
  }

  getPage(event, getURL) {
    event.preventDefault();
    this.ds.getPackages(getURL).subscribe(
      result => {
        this.packages = result;
      }
    );
  }
  //href="{{ apiURL }}packages/{{ package.ID}}/generate-report/"
  downloadReport(event,package_id){
    event.preventDefault();
    let elem = null
    for(let itm of event.path){
      if(/package_btn_\d+/.test(itm.id)){
        elem = itm;
      }
    }
    let elemChild = elem.firstChild;
    elem.innerHTML = '<span class="fa fa-spin fa-spinner"></span>';
    return this.ds.downloadReport(package_id).subscribe(
      (data) => {
        this.openFileForDownload(data, package_id);
        elem.innerHTML = null;
        elem.appendChild(elemChild);
      },
      (error: any) => {
        console.log('О боже ошибка');
      });
  }

  openFileForDownload(data: Response, package_id) {
    //var blob = new Blob([data._body], { type: 'text/csv;charset=utf-8' });
    //saveAs(blob, 'Some.csv');
    let content_type = data.headers.get('Content-type');
    const options = {year: 'numeric', month: 'numeric', day: 'numeric', hour: '2-digit', minute: '2-digit', second: '2-digit'}
    let filename = package_id + '_' + new Date().toLocaleString('ru-RU', options) + '.xml';
    saveAs(data.blob(), filename);
  }

  ngOnInit() {
  }

}
