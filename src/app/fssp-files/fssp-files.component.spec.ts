import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FsspFilesComponent } from './fssp-files.component';

describe('FsspFilesComponent', () => {
  let component: FsspFilesComponent;
  let fixture: ComponentFixture<FsspFilesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FsspFilesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FsspFilesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
