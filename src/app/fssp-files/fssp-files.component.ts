import { Component, OnInit } from '@angular/core';
import { FsspFileService } from '../services/fssp-file/fssp-file.service';
import { Router, ActivatedRoute } from '@angular/router';
import { saveAs } from 'file-saver';

@Component({
  selector: 'app-fssp-files',
  templateUrl: './fssp-files.component.html',
  styleUrls: ['./fssp-files.component.sass']
})
export class FsspFilesComponent implements OnInit {
  fileList = {
    count: null,
    next: null,
    previous: null,
    results: []
  };
  package_id = null;
  loading = true;
  private sub: any;

  constructor(
    private ffs: FsspFileService,
    private router: Router,
    private route: ActivatedRoute
  ) {
    this.sub = this.route.params.subscribe(
      params => {
        this.package_id = params['id'];
        this.getFileList(params['id']);
      }
    );
  }

  downloadFile(event,file_id){
    event.preventDefault();
    let elem = null
    for(let itm of event.path){
      if(/file_btn_\d+/.test(itm.id)){
        elem = itm;
      }
    }
    let elemChildList = [];
    for(let item of elem.children){
      elemChildList.push(item)
    }
    elem.innerHTML = '<span class="fa fa-spin fa-spinner"></span>';
    return this.ffs.downloadFile(file_id).subscribe(
      (data) => {
        this.openFileForDownload(data, file_id);
      },
      (error: any) => {
        elem.innerHTML = null;
        for(let elemChild of elemChildList){
          elem.appendChild(elemChild)
        }
      },
      () => {
        elem.innerHTML = null;
        for(let elemChild of elemChildList){
          elem.appendChild(elemChild)
        }
      }
    );
  }

  openFileForDownload(data: Response, package_id) {
    //var blob = new Blob([data._body], { type: 'text/csv;charset=utf-8' });
    //saveAs(blob, 'Some.csv');
    let content_type = data.headers.get('Content-type');
    const options = {year: 'numeric', month: 'numeric', day: 'numeric', hour: '2-digit', minute: '2-digit', second: '2-digit'}
    let filename = package_id + '_' + new Date().toLocaleString('ru-RU', options) + '.rar';
    saveAs(data.blob(), filename);
  }

  getFileList(file_id, page=null){
    if(!this.loading){
      this.loading = true;
    }
    this.ffs.getFileList(file_id, page).subscribe(
      result => {
        this.fileList = result;
        this.loading = false;
      }
    );
  }

  hasNextPage(event) {
    event.preventDefault();
    if(this.fileList.next) {
      this.getFileList(this.package_id, this.fileList.next);
    }
  }

  hasPreviousPage(event) {
    event.preventDefault();
    if(this.fileList.previous) {
      this.getFileList(this.package_id, this.fileList.previous);
    }
  }

  ngOnInit() {
  }

}
