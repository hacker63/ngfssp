import { Component, OnInit } from '@angular/core';
import { AuthService } from '../services/auth/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.sass']
})
export class NavbarComponent implements OnInit {

  userInfo = {
    last_name: null,
    first_name: null
  };

  constructor(private authService: AuthService,
              private router: Router) {
    this.userInfo = authService.userInfo;
  }

  ngOnInit() {
  }

  logout(event) {
    event.preventDefault();
    this.authService.logout();
    this.router.navigate(['/login']);
  }

}
